type alphabet = A | B

let print_alphabet ff alph =
  let str =
    match alph with
    | A -> "A"
    | B -> "B" in
  Format.fprintf ff "%s" str

let test1 =
  [B; A; A; B; A; B; B; B; A; B; B]

let test2 =
  [A; A; A; B; A; A; B; A]
