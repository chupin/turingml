(** The direction the needle moves *)
type dir = L | R

(** The word initially written on the tape is made of values from an
   alphabet 'alph, but the set of tape symbols is larger than that.
   *)
type ('alph, 'symb) tape_symbol =
  | Alph of 'alph
  | Symb of 'symb

(** A Turing machine is given by an alphabet, some extra symbols (the
   union of the two making the set of tape symbols) and a set of
   possible state. We also have an initial state, a list of state the
   machine « accepts » to terminate successfuly and a transition
   function.

   The transition function computes from the current state and the
   current symbol (the blank symbol is denoted with None) a new state
   of the machine, a new symbol to put on the tape (again, None to
   write a blank symbol) and the direction to move the needle to.

   The transition function may throw a Reject exception to signify it
   doesn't accept the current word. *)
module type TuringSig = sig
  type alphabet
  type symb
  type state
  val init : state
  val accept : state list
  val transition : state ->
                   (alphabet, symb) tape_symbol option ->
                   (state * (alphabet, symb) tape_symbol option * dir)
  exception Reject

  val print_state : Format.formatter -> state -> unit
  val print_alphabet : Format.formatter -> alphabet -> unit
  val print_symb : Format.formatter -> symb -> unit
end

(** The machine output after a step. If it terminates, it tells us
   which state we have accepted. Otherwise it tells us if we are not
   done running, or if we have crashed. *)
type 'state res =
  | Accept of 'state
  | Reject
  | NotDone

module Machine(T : TuringSig) = struct
  type t =
    { tape: (int, (T.alphabet, T.symb) tape_symbol) Hashtbl.t
    ; mutable head: int
    ; mutable state: T.state
    }

  (** Function to print the state of the machine, given ways to print
     the state and tape symbols. *)
  let print_machine ff machine =
    let open Format in
    let ptape head ff tape =
      let (min_key, max_key) =
        Hashtbl.fold (fun idx _ (cmin, cmax) -> (min idx cmin, max idx cmax))
          tape (machine.head, machine.head) in
      let with_head key pv ff v =
        if key = head
        then fprintf ff "@[ >%a< @]" pv v
        else fprintf ff "@[  %a  @]" pv v in
      let print_alph ff = function
        | None -> fprintf ff " "
        | Some(Alph a) -> T.print_alphabet ff a
        | Some(Symb s) -> T.print_symb ff s in
      for key = min_key to max_key do
          let symb =
            try
              Some(Hashtbl.find tape key)
            with
            | Not_found -> None in
          fprintf ff "@[|%a|@]" (with_head key print_alph) symb
      done in
    fprintf ff "@[<v0>@[State: %a@]@,@[%a@]@]" T.print_state machine.state (ptape machine.head) machine.tape

  (** Initialize the machine with a list of alphabet symbols. The
     first symbol is written at position 0. *)
  let init str =
    let tape = Hashtbl.create (2 * List.length str) in
    List.iteri (fun idx char -> Hashtbl.add tape idx (Alph char)) str;
    { head = 0;
      state = T.init;
      tape = tape }

  (** Run one step of computation. Returns whether the machine has
     terminated (successfully or unsuccessfuly) or not. *)
  let step machine =
    let curr_char =
      try
        Some(Hashtbl.find machine.tape machine.head)
      with
      | Not_found -> None in
    if List.mem machine.state T.accept
    then Accept(machine.state)
    else
      try
        let new_state, new_char, dir = T.transition machine.state curr_char in
        begin
          match new_char with
          | None -> Hashtbl.remove machine.tape machine.head
          | Some(char) -> Hashtbl.add machine.tape machine.head char
        end;
        machine.state <- new_state;
        begin
          match dir with
          | L -> machine.head <- machine.head - 1
          | R -> machine.head <- machine.head + 1
        end;
        NotDone
      with | T.Reject -> Reject

  let rec run_until_stop cwm =
    Format.printf "%a\n@." print_machine cwm;
    match step cwm with
    | NotDone -> run_until_stop cwm
    | Reject -> Format.printf "Rejected\n"
    | Accept s -> Format.printf "Accepted: %a" T.print_state s

end
